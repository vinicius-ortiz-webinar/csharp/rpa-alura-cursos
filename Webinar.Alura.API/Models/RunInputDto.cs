﻿using System.Text.Json.Serialization;

namespace Webinar.Alura.API.Models
{
    public class RunInputDto
    {
        [JsonPropertyName("curso")]
        public string Curso { get; set; } = string.Empty;
    }
}
