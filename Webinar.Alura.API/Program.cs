using System.Text.Json;
using Webinar.Alura.API.Models;
using Webinar.Alura.App;
using Webinar.Alura.App.Infrastructure;
using Webinar.Alura.Persistence;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddInfrastructureServices();
builder.Services.AddPersistenceServices();

builder.Services.AddScoped<IRunner, Runner>();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.MapPost("/run", async (context) =>
{
    if (context.Request.Body is null)
    {
        await context.Response.WriteAsync("Please, provide correct json input");
        return;
    }

    var payload = await JsonSerializer.DeserializeAsync<RunInputDto>(context.Request.Body);
    var runner = context.RequestServices.GetService<IRunner>();
    runner.Run(payload.Curso);
});

app.Run();
