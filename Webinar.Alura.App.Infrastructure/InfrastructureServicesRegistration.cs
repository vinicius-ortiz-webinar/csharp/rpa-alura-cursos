﻿using Microsoft.Extensions.DependencyInjection;
using Webinar.Alura.App.Infrastructure.Services;
using Webinar.Alura.Common.Application.Contracts.Services;
using Webinar.Alura.Common.Application.Contracts.WebDriver;
using Webinar.Alura.Common.Application.Services;
using Webinar.Alura.Common.Infrastructure.Selenium;

namespace Webinar.Alura.App.Infrastructure
{
    public static class InfrastructureServicesRegistration
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection service)
        {
            service.AddScoped<IWebDriverService, SeleniumService>();
            service.AddScoped<IAluraLoginService, AluraLoginService>();
            service.AddScoped<IAluraBuscaService, AluraBuscaService>();

            service.AddSingleton<ICsvService, CsvService>();

            return service;
        }
    }
}