﻿using System.Text;
using Webinar.Alura.Common.Domain.Cursos;

namespace Webinar.Alura.App.Infrastructure.Services
{
    public class CsvService : ICsvService
    {
        public void LogCurso(string file, Curso curso)
        {
            if (!File.Exists(file))
            {
                File.WriteAllText(file, "Titulo;Descricao;Carga;Professor");
            }

            File.AppendAllText(file, $"\n{curso.Titulo};{curso.Descricao};{curso.CargaHoraria};{curso.Professor}", Encoding.UTF8);
        }
    }
}
