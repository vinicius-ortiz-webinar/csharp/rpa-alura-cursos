﻿using Webinar.Alura.Common.Domain.Cursos;

namespace Webinar.Alura.App.Infrastructure.Services
{
    public interface ICsvService
    {
        void LogCurso(string file, Curso curso);
    }
}