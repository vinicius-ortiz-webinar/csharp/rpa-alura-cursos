﻿using System.Text.Json;

namespace Webinar.Alura.App.Extensions
{
    public static class ObjectExtensions
    {
        public static string ToSerialize(this object obj)
        {
            return JsonSerializer.Serialize(obj);
        }

        public static string ToQueryString(this object obj)
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name.ToLower() + "=" + p.GetValue(obj, null).ToString().ToLower();
            return string.Join("&", properties.ToArray());
        }
    }
}
