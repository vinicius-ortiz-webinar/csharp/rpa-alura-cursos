﻿namespace Webinar.Alura.App
{
    public interface IRunner
    {
        void Run(string curso = "RPA");
    }
}
