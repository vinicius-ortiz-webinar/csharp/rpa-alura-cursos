﻿using Microsoft.Extensions.DependencyInjection;
using Webinar.Alura.App;
using Webinar.Alura.App.Infrastructure;
using Webinar.Alura.Persistence;

var serviceCollection = new ServiceCollection();
serviceCollection.AddInfrastructureServices();
serviceCollection.AddPersistenceServices();


serviceCollection.AddScoped<IRunner, Runner>();

serviceCollection.BuildServiceProvider()
    .GetService<IRunner>()?.Run();
