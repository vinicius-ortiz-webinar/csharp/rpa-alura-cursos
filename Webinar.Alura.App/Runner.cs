﻿using Microsoft.Extensions.Logging;
using System.Text.Json;
using Webinar.Alura.App.Extensions;
using Webinar.Alura.App.Infrastructure.Services;
using Webinar.Alura.Common.Application.Contracts.Services;
using Webinar.Alura.Common.Domain.Cursos;
using Webinar.Alura.Domain;
using Webinar.Alura.Persistence.Contracts.Repository;

namespace Webinar.Alura.App
{
    public class Runner : IRunner
    {
        private readonly IAluraLoginService _aluraLoginService;
        private readonly IAluraBuscaService _aluraBuscaService;
        private readonly ICsvService _csvService;
        private readonly ICursosRepository _cursosRepository;
        private readonly ILogger<Runner> _logger;

        public Runner(
            IAluraLoginService aluraLoginService,
            IAluraBuscaService aluraBuscaService,
            ICsvService csvService,
            ICursosRepository cursosRepository,
            ILogger<Runner> logger
        )
        {
            _aluraLoginService = aluraLoginService;
            _aluraBuscaService = aluraBuscaService;
            _csvService = csvService;
            _cursosRepository = cursosRepository;
            _logger = logger;
        }

        public void Run(string curso = "RPA")
        {
            try
            {
                _aluraLoginService.Login("", "", "");
                _aluraBuscaService.GoTo();
                _aluraBuscaService.Pesquisar(curso);
                Thread.Sleep(2000);
                var listaUrlCursos = _aluraBuscaService.BuscarListaUrlCursos();

                foreach (var url in listaUrlCursos.Where(u => !u.Contains("cursos.alura")))
                {
                    _logger.LogInformation($"Iniciando url: {url}");
                    var cursoTitulo = string.Empty;
                    var cursoDescricao = string.Empty;
                    double cursoCargaHoraria = 0;
                    var cursoProfessor = string.Empty;
                    var status = string.Empty;
                    var mensagem = string.Empty;
                    try
                    {
                        var cursoAlura = _aluraBuscaService.BuscarDadosCurso(url);
                        cursoTitulo = cursoAlura.Titulo;
                        cursoDescricao = cursoAlura.Descricao;
                        cursoCargaHoraria = cursoAlura.CargaHoraria;
                        cursoProfessor = cursoAlura.Professor;
                        status = "Sucesso";
                        mensagem = string.Empty;
                    }
                    catch (Exception e)
                    {
                        status = "Falha";
                        mensagem = e.Message;
                        _logger.LogError(e, e.Message);
                    }
                    finally
                    {
                        var cursoEntity = new CursoEntity
                        {
                            Titulo = cursoTitulo,
                            CargaHoraria = cursoCargaHoraria,
                            Descricao = cursoDescricao,
                            Professor = cursoProfessor,
                            Url = url,
                            Status = status,
                            Mensagem = mensagem
                        };
                        _logger.LogInformation($"Dados salvados com sucesso: {cursoEntity.ToSerialize()}");
                        _cursosRepository.Create(cursoEntity);
                    }

                }
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
                throw;
            }
            
        }
    }
}
