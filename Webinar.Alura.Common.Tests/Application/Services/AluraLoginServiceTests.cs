﻿using Microsoft.Extensions.Configuration;
using Webinar.Alura.Common.Application.Services;
using Webinar.Alura.Common.Infrastructure.Selenium;

namespace Webinar.Alura.Common.Tests.Application.Services
{
    public class AluraLoginServiceTests
    {
        private AluraLoginService _service;
        private string _username;
        private string _password;
        private string _url;
        private IConfigurationRoot _configuration;

        [SetUp]
        public void Setup()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .AddUserSecrets<AluraLoginServiceTests>();

            _configuration = builder.Build();

            var webdriver = new SeleniumService();
            _service = new AluraLoginService(webdriver);

            _username = _configuration["Alura:Username"];
            _password = _configuration["Alura:Password"];
            _url = "https://cursos.alura.com.br/loginForm";
        }

        [Test]
        public void Login_WhenCalled_ShouldNotReturnError()
        {

            Assert.DoesNotThrow(() =>
            {
                _service.Login(_url, _username, _password);
            });
        }
    }
}
