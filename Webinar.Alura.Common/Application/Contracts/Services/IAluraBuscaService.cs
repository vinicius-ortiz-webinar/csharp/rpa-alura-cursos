﻿using Webinar.Alura.Common.Domain.Cursos;

namespace Webinar.Alura.Common.Application.Contracts.Services
{
    public interface IAluraBuscaService
    {
        void GoTo();
        void Pesquisar(string text);
        IEnumerable<string> BuscarListaUrlCursos();
        Curso BuscarDadosCurso(string url);
    }
}
