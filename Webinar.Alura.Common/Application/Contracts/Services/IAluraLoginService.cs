﻿namespace Webinar.Alura.Common.Application.Contracts.Services
{
    public interface IAluraLoginService
    {
        void Login(string url, string email, string senha);
    }
}
