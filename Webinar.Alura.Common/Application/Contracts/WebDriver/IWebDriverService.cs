﻿using OpenQA.Selenium;

namespace Webinar.Alura.Common.Application.Contracts.WebDriver
{
    public interface IWebDriverService
    {
        void InicializarChrome();
        void GoToUrl(string url);
        IWebElement GetElement(By by, int timeoutInSeconds = 10);
        IWebElement? TryGetElement(By by, int timeoutInSeconds = 10);

        IEnumerable<IWebElement> GetElements(By by, int timeoutInSeconds = 10);
    }
}
