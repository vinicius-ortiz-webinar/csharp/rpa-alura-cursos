﻿using OpenQA.Selenium;
using Webinar.Alura.Common.Application.Contracts.Services;
using Webinar.Alura.Common.Application.Contracts.WebDriver;
using Webinar.Alura.Common.Domain.Cursos;

namespace Webinar.Alura.Common.Application.Services
{
    public class AluraBuscaService : IAluraBuscaService
    {
        private readonly IWebDriverService _webDriverService;

        public AluraBuscaService(IWebDriverService webDriverService)
        {
            _webDriverService = webDriverService;
        }

        public void GoTo()
        {
            _webDriverService.GoToUrl("https://www.alura.com.br/busca");
        }

        public void Pesquisar(string text)
        {
            var by = By.Id("busca-form-input");
            var inputElement = _webDriverService.GetElement(by);
            inputElement.SendKeys(text);
            inputElement.SendKeys(Keys.Enter);

        }

        public IEnumerable<string> BuscarListaUrlCursos()
        {
            var by = By.ClassName("busca-resultado-link");
            var elementsList = _webDriverService.GetElements(by);
            return elementsList.Select(e => e.GetAttribute("href")).ToList();
        }

        public Curso BuscarDadosCurso(string url)
        {
            _webDriverService.GoToUrl(url);

            var by = By.XPath("//h1[@class = 'curso-banner-course-title']");
            var tituloElement = _webDriverService.TryGetElement(by,2);
            if (tituloElement is null)
            {
                by = By.XPath("//h1[@class = 'formacao-headline-titulo']");
                tituloElement = _webDriverService.GetElement(by);
            }

            by = By.XPath("//p[@class = 'course--banner-text-category']");
            var descricaoElement = _webDriverService.TryGetElement(by, 1);
            if (descricaoElement is null)
            {
                by = By.XPath("//div[@class = 'formacao-descricao-texto']/p");
                descricaoElement = _webDriverService.GetElement(by, 1);
            }
            
            by = By.XPath("//div[p[contains(text(), 'Para conclusão')]]/p[@class='courseInfo-card-wrapper-infos']");
            var cargaHoraria = _webDriverService.TryGetElement(by, 1);
            if(cargaHoraria is null)
            {
                by = By.XPath("//div[p[contains(text(), 'Para conclusão')]]/div");
                cargaHoraria = _webDriverService.GetElement(by, 1);
            }

            by = By.XPath("//h3[@class = 'instructor-title--name']");
            var professorElement = _webDriverService.TryGetElement(by, 1);
            var professoreNomes = professorElement is not null ? professorElement.Text : string.Empty;
            if (professorElement is null)
            {
                by = By.XPath("//h3[@class = 'formacao-instrutor-nome']");
                var professoresElement = _webDriverService.GetElements(by);
                var professoresLista = professoresElement.Select(p => p.Text).Distinct().Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
                professoreNomes = string.Join(", ", professoresLista);
            }

            return new Curso
            {
                Titulo = tituloElement.Text,
                Descricao = descricaoElement.Text,
                CargaHoraria = double.Parse(cargaHoraria.Text.Replace("h", "")),
                Professor = professoreNomes
            };

        }
    }
}
