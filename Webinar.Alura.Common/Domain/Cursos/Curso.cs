﻿namespace Webinar.Alura.Common.Domain.Cursos
{
    public class Curso
    {
        public string Titulo { get; set; } = string.Empty;
        public string Descricao { get; set; } = string.Empty;
        public double CargaHoraria { get; set; }
        public string Professor { get; set; } = string.Empty;
    }
}
