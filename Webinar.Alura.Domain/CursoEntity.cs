﻿namespace Webinar.Alura.Domain
{
    public class CursoEntity : BaseEntity<int>
    {
        public string Titulo { get; set; } = string.Empty;
        public string Descricao { get; set; } = string.Empty;
        public double CargaHoraria { get; set; }
        public string Professor { get; set; } = string.Empty;
        public string Url { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public string Mensagem { get; set; } = string.Empty;
    }

}