﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Webinar.Alura.Domain;

namespace Webinar.Alura.Persistence.Configurations
{
    internal class CursosConfiguration : IEntityTypeConfiguration<CursoEntity>
    {
        public void Configure(EntityTypeBuilder<CursoEntity> builder)
        {
            builder.ToTable("Cursos");

            builder
                .HasKey(c => c.Id);

            builder
                .Property(c => c.CargaHoraria)
                .HasColumnType("float");

        }
    }
}
