﻿using System.Linq.Expressions;
using Webinar.Alura.Domain;

namespace Webinar.Alura.Persistence.Contracts.Repository.Common
{
    public interface IRepositoryBase<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        IReadOnlyList<TEntity> Get();
        TEntity GetById(TKey id);
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Delete(TEntity entity);
        TEntity GetFromQuery(Expression<Func<TEntity, bool>> query);
    }
}
