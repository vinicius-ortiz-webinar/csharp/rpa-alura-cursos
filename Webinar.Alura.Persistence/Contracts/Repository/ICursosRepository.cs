﻿using Webinar.Alura.Domain;
using Webinar.Alura.Persistence.Contracts.Repository.Common;

namespace Webinar.Alura.Persistence.Contracts.Repository
{
    public interface ICursosRepository : IRepositoryBase<CursoEntity, int>
    {
    }
}
