﻿using Microsoft.EntityFrameworkCore;
using Webinar.Alura.Domain;

namespace Webinar.Alura.Persistence.DatabaseContexts
{
    public class AluraContext : DbContext
    {
        public AluraContext(DbContextOptions<AluraContext> options) : base(options){}

        public DbSet<CursoEntity> Cursos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AluraContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var entry in base.ChangeTracker.Entries<BaseEntity<dynamic>>()
                .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.CreatedAt = DateTime.Now;
                }
                entry.Entity.UpdatedAt = DateTime.Now;
            }
            return base.SaveChanges();
        }
    }
}
