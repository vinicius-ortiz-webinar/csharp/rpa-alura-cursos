﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Webinar.Alura.Persistence.Contracts.Repository;
using Webinar.Alura.Persistence.Contracts.Repository.Common;
using Webinar.Alura.Persistence.DatabaseContexts;
using Webinar.Alura.Persistence.Repository;
using Webinar.Alura.Persistence.Repository.Common;

namespace Webinar.Alura.Persistence
{
    public static class PersistenceServiceRegistration
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services)
        {
            services.AddDbContext<AluraContext>(options =>
            {
                options.UseSqlite("Data Source=mydatabase.db");
            });

            services.AddScoped(typeof(IRepositoryBase<,>), typeof(RepositoryBase<,>));
            services.AddScoped<ICursosRepository, CursosRepository>();
            return services;
        }
    }
}
