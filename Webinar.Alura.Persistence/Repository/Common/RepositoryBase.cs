﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Webinar.Alura.Domain;
using Webinar.Alura.Persistence.Contracts.Repository.Common;
using Webinar.Alura.Persistence.DatabaseContexts;

namespace Webinar.Alura.Persistence.Repository.Common
{
    public class RepositoryBase<TEntity, TKey> : IRepositoryBase<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        private readonly AluraContext _context;
        protected DbSet<TEntity> _dbSet;

        public RepositoryBase(AluraContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public TEntity Create(TEntity entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public TEntity Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
            _context.SaveChanges();
            return entity;
        }

        public IReadOnlyList<TEntity> Get()
        {
            return _dbSet.AsNoTracking().ToList();
        }

        public TEntity GetById(TKey id)
        {
            return _dbSet.AsNoTracking().FirstOrDefault(q => q.Id.Equals(id));
        }

        public TEntity GetFromQuery(Expression<Func<TEntity, bool>> query)
        {
            return _dbSet.AsNoTracking().FirstOrDefault(query);
        }

        public TEntity Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
            return entity;
        }
    }
}
