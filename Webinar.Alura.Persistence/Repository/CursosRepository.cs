﻿using Webinar.Alura.Domain;
using Webinar.Alura.Persistence.Contracts.Repository;
using Webinar.Alura.Persistence.DatabaseContexts;
using Webinar.Alura.Persistence.Repository.Common;

namespace Webinar.Alura.Persistence.Repository
{
    public class CursosRepository : RepositoryBase<CursoEntity, int>, ICursosRepository
    {
        public CursosRepository(AluraContext context) : base(context)
        {
        }

    }
}
